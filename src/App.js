import React, { Component } from 'react';
import moment from 'moment';

import { getCopy } from './shared/utils.js';

import Page from './hoc/Page/Page';
import PlanPage from './containers/PlanPage/PlanPage';

import fetchMore from './data.js';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            settings: {},
            days: {},
        };
        this.callback = this.callback.bind(this);
        this.onFetchMore = this.onFetchMore.bind(this);
    }

    componentDidMount() {
        const firstDay = moment()
            .subtract(2, 'days')
            .format('YYYY-MM-DD');

        Promise.all([
            import('./settings.js'),
            fetchMore(firstDay, 'forward', 9),
        ]).then(data => {
            this.setState({
                settings: data[0].default,
                days: data[1],
            });
        });
    }

    callback(type, update) {
        this.setState(state => {
            const { date, time } = update;
            let day = getCopy(state.days[date]);

            if (type === 'update') {
                if (day === undefined) {
                    day = { posts: { [time]: update.post } };
                } else {
                    day.posts = {
                        ...day.posts,
                        [time]: update.post,
                    };
                }
            }

            if (type === 'delete') {
                delete day.posts[time];
            }

            return { days: { ...state.days, [date]: day } };
        });
    }

    onFetchMore(date, direction) {
        fetchMore(date, direction, 7).then(data =>
            this.setState({ days: { ...this.state.days, ...data } })
        );
    }

    render() {
        return (
            <Page>
                <PlanPage
                    settings={this.state.settings}
                    days={this.state.days}
                    callback={this.callback}
                    onFetchMore={this.onFetchMore}
                />
            </Page>
        );
    }
}

export default App;
