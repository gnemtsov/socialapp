import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import ErrorBoundary from '../../hoc/ErrorBoundary/ErrorBoundary';
import { getCopy } from '../../shared/utils';

import Day from '../../components/Day/Day';
import Popup from '../../hoc/Popup/Popup';
import PostForm from '../../components/PostForm/PostForm';

import classes from './PlanPage.module.scss';

class PlanPage extends Component {
    static propTypes = {
        days: PropTypes.object,
        callback: PropTypes.func.isRequired,
        onFetchMore: PropTypes.func.isRequired,
    };

    //utility functions
    static getDaySettings = (daysSettings, date) => ({
        retweets: '0',
        likes: '0',
        replies: '0',
        slots: {},
        ...daysSettings['default'],
        ...daysSettings[moment(date).format('dddd')],
        ...daysSettings[date],
    });

    constructor(props) {
        super(props);

        this.state = {
            popup: {
                open: false,
                title: '',
                data: {},
            },
            loading: {
                before: null,
                after: null,
            },
        };
    }

    getOccupiedTimes = date => {
        const day = this.props.days[date];
        let occupiedTimes = {};

        if (day !== undefined && day.posts !== undefined) {
            const times = Object.keys(day.posts);
            times.forEach(time => {
                const [hour, minute] = time.split(':');
                occupiedTimes[hour] = {
                    ...occupiedTimes[hour],
                    [minute]: true,
                };
            });
        }

        return occupiedTimes;
    };

    //lifecycle methods
    componentDidMount() {
        window.addEventListener('scroll', this.scrollHandler);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.scrollHandler);
    }

    //event handlers
    cardClickHandler = (date, time) => {
        this.setState((state, props) => {
            let title = 'New post';
            let canDelete = false;
            let post = {
                content: '',
                networks: {},
            };

            if (
                time !== undefined &&
                props.days[date] !== undefined &&
                props.days[date].posts !== undefined &&
                props.days[date].posts[time] !== undefined
            ) {
                title = 'Edit post';
                canDelete = moment(
                    date + ' ' + time,
                    'YYYY-MM-DD HH:mm'
                ).isAfter(moment());
                post = getCopy(props.days[date].posts[time]);
            }

            post.date = date;

            if (time !== undefined) {
                post.hour = time.split(':')[0];
                post.minute = time.split(':')[1];
            } else {
                post.hour = moment().format('HH');
                post.minute = moment()
                    .add(10, 'minutes')
                    .format('mm');
            }

            const formKey = moment().format('x');

            return {
                popup: {
                    open: true,
                    title,
                    formKey,
                    canDelete,
                    post,
                },
            };
        });
    };

    closePopup = () => {
        this.setState({ popup: { open: false } });
    };

    deleteHandler = post => {
        const data = {
            date: post.date,
            time: post.hour + ':' + post.minute,
        };
        this.props.callback('delete', data);
        this.closePopup();
    };

    submitHandler = post => {
        const data = {
            date: post.date,
            time: post.hour + ':' + post.minute,
            post: {
                content: post.content,
                networks: { ...post.networks },
            },
        };
        this.props.callback('update', data); //update means create or edit
        this.closePopup();
    };

    scrollHandler = event => {
        const el = event.target.documentElement || event.target.documentElement;
        const { scrollTop, scrollHeight, clientHeight } = el;

        //fetch after
        //"clientHeight*n" - start fetching before bottom of the page is reached
        if (
            this.state.loading.after !== this.lastDay &&
            scrollTop + clientHeight * 3 >= scrollHeight
        ) {
            this.setState(state => {
                return {
                    loading: {
                        ...state.loading,
                        after: this.lastDay,
                    },
                };
            });
            this.props.onFetchMore(
                moment(this.lastDay)
                    .add(1, 'days')
                    .format('YYYY-MM-DD'),
                'forward'
            );
        }

        //fetch before
        //detect scroll position is on top + was moving up
        if (
            this.state.loading.before !== this.firstDay &&
            scrollTop < 10 &&
            this.prevScrollTop > scrollTop
        ) {
            this.setState(state => {
                return {
                    loading: {
                        ...state.loading,
                        before: this.firstDay,
                    },
                };
            });
            this.props.onFetchMore(
                moment(this.firstDay)
                    .subtract(1, 'days')
                    .format('YYYY-MM-DD'),
                'backward'
            );
        }

        this.prevScrollTop = scrollTop;
    };

    render() {
        const loader = <div>Loading...</div>;

        if (
            this.props.days === undefined ||
            this.props.days === null ||
            (typeof this.props.days === 'object' &&
                Object.keys(this.props.days).length === 0)
        ) {
            return <div className={classes.PlanPage}>{loader}</div>;
        }

        /*
        Auto-scrolling feature
        scrollToMe and scrollToMeOnce are used to tell the day whether it should scroll document to itself when it is mounted. "Today" scrolls to its top. The day before "before loader" scrolls to its bottom.
        */

        //skip empty days in the past, sort, setup initial scrolling
        let dates = Object.keys(this.props.days)
            .filter(date => {
                const day = this.props.days[date];
                return (
                    Object.keys(day).length !== 0 ||
                    moment(date).isSameOrAfter(moment(), 'day')
                );
            })
            .sort()
            .map(date => {
                let dateObj = { value: date };

                if (
                    this.state.loading.before !== null &&
                    moment(date).isBefore(
                        moment(this.state.loading.before),
                        'day'
                    )
                ) {
                    dateObj.scrollToMe = 'bottom';
                }

                if (moment(date).isSame(moment(), 'day')) {
                    dateObj.scrollToMe = 'top';
                }
                return dateObj;
            });

        //Create days array for rendering
        //set scrollToMe prop only once
        const { settings } = this.props;
        let scrollToMeOnce = 'virgin';
        let days = [];
        dates.forEach(({ value: date, scrollToMe }, i) => {
            const day = this.props.days[date];
            if (scrollToMeOnce === 'virgin') {
                if (
                    scrollToMe === 'bottom' &&
                    dates[i + 1].scrollToMe !== 'bottom'
                ) {
                    scrollToMeOnce = 'bottom';
                }
                if (scrollToMe === 'top') {
                    scrollToMeOnce = 'top';
                }
            }

            days.push(
                <ErrorBoundary key={date}>
                    <Day
                        date={date}
                        day={day}
                        settings={{
                            dateTitleDisplayFormat:
                                settings.dateTitleDisplayFormat,
                            ...PlanPage.getDaySettings(settings.days, date),
                        }}
                        scrollToMe={scrollToMeOnce}
                        cardClickHandler={this.cardClickHandler}
                    />
                </ErrorBoundary>
            );

            if (scrollToMeOnce !== 'virgin') {
                scrollToMeOnce = 'mature';
            }
        });

        this.firstDay = dates[0].value;
        this.lastDay = dates[dates.length - 1].value;

        let popup = null;
        if (this.state.popup.open) {
            popup = (
                <ErrorBoundary>
                    <Popup
                        title={this.state.popup.title}
                        closeHandler={this.closePopup}
                    >
                        <PostForm
                            key={this.state.popup.formKey}
                            canDelete={this.state.popup.canDelete}
                            initialValues={{ ...this.state.popup.post }}
                            accountsNetworks={settings.accountsNetworks}
                            dateSelectFormat={settings.dateSelectFormat}
                            getOccupiedTimes={this.getOccupiedTimes}
                            deleteHandler={this.deleteHandler}
                            submitHandler={this.submitHandler}
                        />
                    </Popup>
                </ErrorBoundary>
            );
        }

        return (
            <div className={classes.PlanPage}>
                {this.state.loading.before === this.firstDay ? loader : null}
                {days}
                {this.state.loading.after === this.lastDay ? loader : null}
                {popup}
            </div>
        );
    }
}

export default PlanPage;
