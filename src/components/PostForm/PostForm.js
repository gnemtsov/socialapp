import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import memoize from 'memoize-one';

import SocialIcon from '../UI/SocialIcon/SocialIcon';

import classes from './PostForm.module.scss';

//PostForm is
//a fully uncontrolled component with a key
class PostForm extends Component {
    static defaultProps = {
        canDelete: false,
    };

    static propTypes = {
        canDelete: PropTypes.bool,
        initialValues: PropTypes.object,
        accountsNetworks: PropTypes.object.isRequired,
        dateSelectFormat: PropTypes.string.isRequired,
        deleteHandler: PropTypes.func,
        submitHandler: PropTypes.func.isRequired,
    };

    static UTCoffset = (() => {
        let offset = moment().utcOffset();
        let sign = '+';

        if (offset[0] === '-') {
            sign = '-';
            offset = offset.substr(1);
        }

        let hours = Math.floor(offset / 60);
        hours = hours < 10 ? '0' + hours : hours;
        let minutes = offset % 60;
        minutes = minutes < 10 ? '0' + minutes : minutes;

        return `UTC${sign}${hours}:${minutes}`;
    })();

    constructor(props) {
        super(props);
        this.state = {
            ...props.initialValues,
            networks: {
                ...props.initialValues.networks,
            },
        };

        this.textareaRef = React.createRef();
        this.firstFocusElementRef = React.createRef();

        //events handlers
        this.iconClickHandler = this.iconClickHandler.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
        this.updateBlurHandler = this.updateBlurHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
    }

    //utility functions
    static isFormReady = values =>
        values.content !== undefined &&
        values.content.length > 0 &&
        values.networks !== undefined &&
        Object.keys(values.networks).filter(
            account => values.networks[account].length > 0
        ).length > 0;

    getDatesOptions = memoize((date, time) => {
        let postMoment = moment(date + ' ' + time);
        let now = moment();
        let from = moment(date).subtract(7, 'days');
        let till = moment(date).add(14, 'days');

        let options = [];
        if (postMoment.isBefore(now)) {
            options.push(
                <option key={date} value={date}>
                    {postMoment.format(this.props.dateSelectFormat)}
                </option>
            );
        } else {
            if (from.isBefore(now)) {
                from = now;
            }

            let dates = [];
            while (from.isSameOrBefore(till, 'day')) {
                dates.push({
                    value: from.format('YYYY-MM-DD'),
                    display: from.format(this.props.dateSelectFormat),
                });
                from.add(1, 'days');
            }

            options = dates.map(date => (
                <option key={date.value} value={date.value}>
                    {date.display}
                </option>
            ));
        }

        return options;
    });

    getTimeOptions = memoize((date, time, type, occupiedTimes) => {
        const withZero = v => ('0' + v).slice(-2);

        let postMoment = moment(date + ' ' + time);
        let now = moment();

        let format = type === 'hours' ? 'H' : 'm';
        let start = 0;
        let finish = type === 'hours' ? 23 : 59;

        if (postMoment.isBefore(now) && !postMoment.isSame(now, 'hour')) {
            start = postMoment.format(format);
            finish = start;
        } else if (postMoment.isSame(now, 'day')) {
            if (type === 'hours') {
                start = now.format(format);
            } else {
                start = postMoment.isSame(now, 'hour') ? now.format(format) : 0;
            }
        }

        let options = [];
        for (let i = start; i <= finish; i++) {
            let value = withZero(i);

            let disabled = false;
            if (finish !== start) {
                if (type === 'hours') {
                    disabled =
                        occupiedTimes[value] !== undefined &&
                        Object.keys(occupiedTimes[value]).length >= 60;
                } else {
                    const hour = postMoment.format('HH');
                    disabled =
                        occupiedTimes[hour] !== undefined &&
                        Object.keys(occupiedTimes[hour]).includes(value);
                }
            }

            options.push(
                <option key={value} value={value} disabled={disabled}>
                    {value}
                </option>
            );
        }

        return options;
    });

    //lifecycle methods
    componentDidMount() {
        this.textareaRef.current.focus();
    }

    //event handlers
    updateBlurHandler(event) {
        this.firstFocusElementRef.current.focus();
    }

    iconClickHandler(account, network) {
        const { networks } = this.state;

        if (networks[account] === undefined || networks[account].length === 0) {
            networks[account] = network;
        } else {
            let nets = networks[account].split(',');
            if (nets.includes(network)) {
                nets = nets.filter(net => net !== network);
            } else {
                nets.push(network);
            }
            networks[account] = nets.join(',');
        }
        this.setState({ networks });
    }

    changeHandler(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    submitHandler(event) {
        event.preventDefault();
        const type = event.target.getAttribute('data-type');

        if (type === 'update') {
            this.props.submitHandler(this.state);
        } else if (type === 'delete') {
            this.props.deleteHandler(this.state);
        }
    }

    render() {
        const date = this.state.date;
        const time = this.state.hour + ':' + this.state.minute;

        const occupiedTimes = this.props.getOccupiedTimes(date);
        const dateOptions = this.getDatesOptions(date, time);
        const hourOptions = this.getTimeOptions(
            date,
            time,
            'hours',
            occupiedTimes
        );
        const minuteOptions = this.getTimeOptions(
            date,
            time,
            'minutes',
            occupiedTimes
        );

        return (
            <form onSubmit={this.submitHandler}>
                <div
                    key="form-when-to-publish"
                    className={classes.FormWhenToPublish}
                >
                    <span className={classes.NoWrap}>When to publish:</span>
                    <select
                        name="date"
                        className={classes.FormDateSelect}
                        defaultValue={this.state.date}
                        ref={this.firstFocusElementRef}
                        tabIndex="102"
                        onChange={this.changeHandler}
                    >
                        {dateOptions}
                    </select>
                    <span className={classes.NoWrap}>
                        at
                        <select
                            name="hour"
                            className={classes.FormHourSelect}
                            defaultValue={this.state.hour}
                            tabIndex="102"
                            onChange={this.changeHandler}
                        >
                            {hourOptions}
                        </select>
                        <select
                            name="minute"
                            className={classes.FormMinuteSelect}
                            defaultValue={this.state.minute}
                            tabIndex="102"
                            onChange={this.changeHandler}
                        >
                            {minuteOptions}
                        </select>
                        <span className={classes.FormTZ}>
                            {PostForm.UTCoffset}
                        </span>
                    </span>
                </div>
                <div key="form-networks" className={classes.FormNetworks}>
                    {Object.keys(this.props.accountsNetworks)
                        .sort()
                        .map(account => {
                            let nets = this.props.accountsNetworks[
                                account
                            ].split(',');
                            let postNetworks = [];
                            if (this.state.networks[account] !== undefined) {
                                postNetworks = this.state.networks[
                                    account
                                ].split(',');
                            }

                            return nets.map(net => (
                                <SocialIcon
                                    key={account + '-' + net}
                                    type="dynamic"
                                    class={classes.Icon}
                                    account={account}
                                    tabIndex="103"
                                    network={net}
                                    selected={postNetworks.includes(net)}
                                    clickHandler={this.iconClickHandler}
                                />
                            ));
                        })}
                </div>
                <textarea
                    className={classes.FormContent}
                    name="content"
                    ref={this.textareaRef}
                    onChange={this.changeHandler}
                    tabIndex="104"
                    placeholder="Text and links"
                    value={this.state.content}
                />
                <div key="form-footer" className={classes.FormFooter}>
                    <button
                        type="Button"
                        data-type="update"
                        tabIndex="106"
                        className={classes.FormSubmit}
                        disabled={!PostForm.isFormReady(this.state)}
                        onClick={this.submitHandler}
                        onBlur={this.updateBlurHandler}
                    >
                        SCHEDULE POST
                    </button>
                    {this.props.canDelete ? (
                        <button
                            type="Button"
                            data-type="delete"
                            tabIndex="105"
                            className={classes.FormDelete}
                            onClick={this.submitHandler}
                        >
                            DELETE
                        </button>
                    ) : null}
                </div>
            </form>
        );
    }
}

export default PostForm;
