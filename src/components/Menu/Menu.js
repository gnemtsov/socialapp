import React from 'react';

import classes from './Menu.module.scss';
import DrawerToggle from './SideDrawer/DrawerToggle/DrawerToggle';

const toolbar = props => (
    <header className={classes.Menu}>
        <DrawerToggle clicked={props.drawerToggleClicked} />
        <nav className={classes.Nav}>{null}</nav>
    </header>
);

export default toolbar;
