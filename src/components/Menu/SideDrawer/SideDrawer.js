import React from 'react';

import classes from './SideDrawer.module.scss';
import Backdrop from '../../UI/Backdrop/Backdrop';

const sideDrawer = props => {
    let attachedClasses = [classes.SideDrawer, classes.Close];
    let backdrop = null;
    if (props.open) {
        attachedClasses = [classes.SideDrawer, classes.Open];
        backdrop = <Backdrop clickHandler={props.closed} />;
    }
    return (
        <React.Fragment>
            <div className={attachedClasses.join(' ')}>
                <nav />
            </div>
            {backdrop}
        </React.Fragment>
    );
};

export default sideDrawer;
