import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TransitionGroup from 'react-transition-group/TransitionGroup';
import CSSTransition from 'react-transition-group/CSSTransition';
import moment from 'moment';

import Post from './Post/Post';

import classes from './Day.module.scss';
import ReadersIcon from './assets/readers.svg';
import RetweetsIcon from './assets/retweets.svg';
import LikesIcon from './assets/likes.svg';
import RepliesIcon from './assets/replies.svg';

class Day extends Component {
    static defaultProps = {
        settings: {},
    };

    static propTypes = {
        date: PropTypes.string,
        day: PropTypes.object,
        settings: PropTypes.object,
        scrollToMe: PropTypes.string,
        cardClickHandler: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        this.ref = React.createRef();
    }

    componentDidMount() {
        if (this.props.scrollToMe !== undefined && this.ref.current !== null) {
            if (this.props.scrollToMe === 'top') {
                document.documentElement.scrollTop =
                    this.ref.current.getBoundingClientRect().top +
                    document.documentElement.scrollTop;
            } else if (this.props.scrollToMe === 'bottom') {
                document.documentElement.scrollTop =
                    this.ref.current.getBoundingClientRect().bottom +
                    document.documentElement.scrollTop -
                    80;
            }
        }
    }

    render() {
        const yesterday = (() => moment().subtract(1, 'days'))();
        const tommorow = (() => moment().add(1, 'days'))();

        const { settings } = this.props;

        //Title and subtitle
        const date = moment(this.props.date);
        const subtitle = date.format('dddd');

        let title;
        if (date.isBetween(yesterday, tommorow, 'day', '[]')) {
            title = date.calendar().split(' ')[0];
        } else {
            title = date.format(settings.dateTitleDisplayFormat);
        }

        //Day summary
        let summary = [
            {
                name: 'readers',
                icon: ReadersIcon,
                value: this.props.day.readers,
            },
            {
                name: 'retweets',
                icon: RetweetsIcon,
                value: this.props.day.retweets,
            },
            {
                name: 'likes',
                icon: LikesIcon,
                value: this.props.day.likes,
            },
            {
                name: 'replies',
                icon: RepliesIcon,
                value: this.props.day.replies,
            },
        ];

        summary = summary
            .filter(({ value }) => value !== undefined)
            .map(({ name, alt, icon, value }) => {
                const percent =
                    settings[name] === undefined
                        ? 0
                        : Math.floor((value / settings[name]) * 100);

                let backgroundImage = [`url('${icon}')`];

                if (percent === 0) {
                    backgroundImage.push(
                        'linear-gradient(90deg, #cdcccc 0%, #cdcccc 100%)'
                    );
                } else if (percent < 100) {
                    let angle =
                        percent <= 50 ? percent * 3.6 + 90 : percent * 3.6 - 90;

                    backgroundImage.push(
                        `linear-gradient(${angle}deg, ${
                            percent <= 50
                                ? 'rgba(205,204,204,0)'
                                : 'rgba(42,42,42,0)'
                        } 49%, ${percent <= 50 ? '#cdcccc' : '#2a2a2a'} 51%)`
                    );
                    backgroundImage.push(
                        'linear-gradient(90deg, #cdcccc 49%, rgba(205,204,204,0) 51%)'
                    );
                }

                backgroundImage = backgroundImage.join(',');
                return (
                    <div key={name} className={classes.Icon} data-text={value}>
                        <div
                            className={classes.IconImg}
                            style={{ backgroundImage }}
                        />
                    </div>
                );
            });

        //Posts
        let posts = {};
        let today = moment();

        if (settings.slots !== undefined) {
            let slotTimes = Object.keys(settings.slots);
            slotTimes.forEach(time => {
                if (moment(this.props.date + ' ' + time).isAfter(today)) {
                    posts[time] = settings.slots[time];
                }
            });
        }

        posts = { ...posts, ...this.props.day.posts };
        let times = Object.keys(posts);

        posts = times.sort().map(time => (
            <CSSTransition
                key={time}
                timeout={800}
                classNames={{
                    enter: '',
                    enterActive: classes.CardAdd,
                    exit: '',
                    exitActive: classes.CardDelete,
                }}
            >
                <Post
                    time={time}
                    clickHandler={() =>
                        this.props.cardClickHandler(this.props.date, time)
                    }
                    {...posts[time]}
                />
            </CSSTransition>
        ));

        let newPost = null;
        if (moment(this.props.date).isSameOrAfter(today, 'day')) {
            newPost = (
                <CSSTransition key="new" timeout={0} classNames={''}>
                    <Post
                        clickHandler={() =>
                            this.props.cardClickHandler(this.props.date)
                        }
                    />
                </CSSTransition>
            );
        }

        //Render
        if (posts.length === 0 && newPost === null) {
            return null;
        } else {
            return (
                <section className={classes.Day} ref={this.ref}>
                    <header className={classes.Header}>
                        <h1 className={classes.Title}>{title}</h1>
                        <div className={classes.SubTitle}>{subtitle}</div>
                        <div className={classes.Summary}>{summary}</div>
                    </header>
                    <TransitionGroup className={classes.Posts}>
                        {posts}
                        {newPost}
                    </TransitionGroup>
                </section>
            );
        }
    }
}

export default Day;
