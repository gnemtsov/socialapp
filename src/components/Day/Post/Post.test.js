import React from 'react';
import { configure, shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';

import Post from './Post.js';

import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

const getPost = (content, time, network) => {
	const clickHandler = jest.fn();
	const wrapper = shallow (
		<Post
			content={content}
			time={time}
			network={network}
			clickHandler={clickHandler}
		/>
	);
	const article = wrapper.find('article');
	return {
		wrapper,
		article,
		clickHandler
	};
};

describe('Post', () => {
	it('renders new post correctly', () => {
		const { wrapper } = getPost(undefined, undefined, undefined);
		expect(shallowToJson(wrapper)).toMatchSnapshot();
	});
	
	it('renders time slot correctly', () => {
		const { wrapper } = getPost(undefined, '10:00', undefined);
		expect(shallowToJson(wrapper)).toMatchSnapshot();
	});
	
	it('renders existing post correctly', () => {
		const { wrapper } = getPost('abcdefg', '10:00', {tesla: 'facebook,gplus'});
		expect(shallowToJson(wrapper)).toMatchSnapshot();
	});
	
	it('handles click', () => {
		// Expect to handle click in all modes (new post, time slot, existing post)
		const params = [
			[undefined, undefined, undefined],
			[undefined, '10:00', undefined],
			['abcdefg', '10:00', {tesla: 'facebook,gplus'}]
		];
		for (const p of params) {
			const { article, clickHandler } = getPost(undefined, undefined, undefined);
			expect(clickHandler.mock.calls.length).toBe(0);
			article.simulate('click');
			expect(clickHandler.mock.calls.length).toBe(1);
		}
	});
});
