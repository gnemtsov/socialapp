import React, { Component } from 'react';
import AnimateOnChange from 'react-animate-on-change';
import TransitionGroup from 'react-transition-group/TransitionGroup';
import CSSTransition from 'react-transition-group/CSSTransition';
import PropTypes from 'prop-types';

import { compareArrays } from '../../../shared/utils';

import SocialIcon from '../../UI/SocialIcon/SocialIcon';

import classes from './Post.module.scss';
import PlusIcon from './assets/plus.svg';

class Post extends Component {
    static propTypes = {
        content: PropTypes.string,
        time: PropTypes.string,
        networks: PropTypes.object,
        clickHandler: PropTypes.func.isRequired,
    };

    state = {
        contentAnimation: false,
    };

    turnContentAnimationOff = () => {
        this.setState({ contentAnimation: false });
    };

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.content !== nextProps.content) {
            this.setState({ contentAnimation: true });
            return true;
        }

        if (
            this.props.networks === undefined ||
            nextProps.networks === undefined
        ) {
            return true;
        }

        let accounts1 = Object.keys(this.props.networks);
        let accounts2 = Object.keys(nextProps.networks);

        if (!compareArrays(accounts1, accounts2)) {
            return true;
        }

        accounts1.filter(
            account =>
                this.props.networks[account] !== nextProps.networks[account]
        );

        if (accounts1.length > 0) {
            return true;
        }

        return false;
    }

    mouseEnterHandler = event => {
        event.currentTarget.focus();
    };

    mouseLeaveHandler = event => {
        event.currentTarget.blur();
    };

    keyDownHandler = event => {
        if (event.keyCode === 13) {
            event.preventDefault();
            event.currentTarget.click();
        }
    };

    render() {
        let post = null;
        if (this.props.networks !== undefined) {
            //existing post
            let accounts = Object.keys(this.props.networks);
            accounts.sort();
            post = (
                <article
                    className={classes.Post}
                    tabIndex="0"
                    onClick={this.props.clickHandler}
                    onMouseEnter={this.mouseEnterHandler}
                    onMouseLeave={this.mouseLeaveHandler}
                    onKeyDown={this.keyDownHandler}
                >
                    <header className={classes.PostHeader}>
                        <time className={classes.PostTime}>
                            {this.props.time}
                        </time>
                        <TransitionGroup className={classes.PostIcons}>
                            {accounts.map(account => {
                                let nets = this.props.networks[account].split(
                                    ','
                                );
                                return nets.map(net => (
                                    <CSSTransition
                                        key={account + '-' + net}
                                        timeout={1000}
                                        classNames={{
                                            enter: '',
                                            enterActive: 'animated bounceIn',
                                            exit: '',
                                            exitActive: 'animated bounceIn',
                                        }}
                                    >
                                        <SocialIcon
                                            type="static"
                                            account={account}
                                            network={net}
                                        />
                                    </CSSTransition>
                                ));
                            })}
                        </TransitionGroup>
                    </header>
                    <AnimateOnChange
                        customTag="div"
                        baseClassName={classes.PostContent}
                        animationClassName={'animated bounceIn'}
                        animate={this.state.contentAnimation}
                        onAnimationEnd={this.turnContentAnimationOff}
                    >
                        {this.props.content}
                    </AnimateOnChange>
                </article>
            );
        } else if (this.props.time !== undefined) {
            //time slot
            post = (
                <article
                    className={classes.PostSlot}
                    tabIndex="0"
                    onClick={this.props.clickHandler}
                    onMouseEnter={this.mouseEnterHandler}
                    onMouseLeave={this.mouseLeaveHandler}
                    onKeyDown={this.keyDownHandler}
                >
                    <header className={classes.PostHeader}>
                        <time>{this.props.time}</time>
                    </header>
                    <AnimateOnChange
                        customTag="div"
                        baseClassName={classes.PostContent}
                        animationClassName={'animated flash'}
                        animate={this.state.contentAnimation}
                        onAnimationEnd={this.turnContentAnimationOff}
                    >
                        {this.props.content}
                    </AnimateOnChange>
                    <img
                        className={classes.PlusIcon}
                        src={PlusIcon}
                        alt="Add a post"
                    />
                </article>
            );
        } else {
            //add new post
            post = (
                <article
                    className={classes.PostSlot}
                    tabIndex="0"
                    onClick={this.props.clickHandler}
                    onMouseEnter={this.mouseEnterHandler}
                    onMouseLeave={this.mouseLeaveHandler}
                    onKeyDown={this.keyDownHandler}
                >
                    <div className={classes.PostContent}>
                        Schedule post
                        <br />
                        on this day
                    </div>
                    <img
                        className={classes.PlusIcon}
                        src={PlusIcon}
                        alt="Add a post"
                    />
                </article>
            );
        }

        return post;
    }
}

export default Post;
