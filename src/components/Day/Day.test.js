import React from 'react';
import moment from 'moment';
import { configure, shallow, mount } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';

import Day from './Day.js';
import Post from './Post/Post.js';

import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

const getDay = day => {
    const cardClickHandler = jest.fn();
    const settings = {
        dateTitleDisplayFormat: 'MMMM, D',
        retweets: '60',
        likes: '200',
        replies: '100',
        slots: {
            '10:00': {
                content: `First slot`,
            },
            '14:00': {
                content: `Second slot`,
            },
            '23:05': {
                content: `Third slot`,
            },
        },
    };
    const wrapper = shallow(
        <Day
            date={moment().add(1, 'days').format('YYYY-MM-DD')}
            day={day}
            settings={settings}
            cardClickHandler={cardClickHandler}
        />
    );
    const posts = wrapper.find(Post);
    return {
        wrapper,
        posts,
        cardClickHandler,
    };
};

const getPosts = () => {
    return {
        '00:05': {
            content: 'abc',
            networks: {
                tesla: 'facebook,gplus',
            },
        },
        '03:45': {
            content: 'def',
            networks: {
                tesla: 'facebook,gplus',
            },
        },
        '12:05': {
            content: 'ghij',
            networks: {
                tesla: 'gplus,twitter',
            },
        },
        '18:55': {
            content: 'klm',
            networks: {
                tesla: 'facebook,gplus',
            },
        },
    };
};

describe('Day', () => {
    it('renders correctly empty', () => {
        const { wrapper, posts } = getDay({});
        expect(shallowToJson(wrapper)).toMatchSnapshot();
        expect(posts).toHaveLength(4);
    });

    it('renders correctly without likes, replies, etc.', () => {
        const day = {
            posts: getPosts(),
        };
        const { wrapper } = getDay(day);
        expect(shallowToJson(wrapper)).toMatchSnapshot();
    });

    it('renders correctly with likes, replies, etc.', () => {
        const day = {
            posts: getPosts(),
            readers: '+100',
            retweets: 60,
            likes: 80,
            replies: 36,
        };
        const { wrapper, posts } = getDay(day);
        expect(shallowToJson(wrapper)).toMatchSnapshot();
        expect(posts).toHaveLength(8);
    });

    it('handles click', () => {
        const day = {
            posts: {
                '06:12': {
                    content: 'abc',
                    networks: {
                        tesla: 'facebook,gplus',
                    },
                },
                '10:00': {
                    content: 'def',
                    networks: {
                        tesla: 'facebook,gplus',
                    },
                },
                '19:12': {
                    content: 'ghij',
                    networks: {
                        tesla: 'gplus,twitter',
                    },
                },
            },
        };

        const { wrapper, posts, cardClickHandler } = getDay(day);

        // Check that calling clickHandler on post calls cardClickHandler with correct parameters
        const date = moment().add(1, 'days').format('YYYY-MM-DD');
        const expectedParams = [
            [date, '06:12'],
            [date, '10:00'],
            [date, '14:00'],
            [date, '19:12'],
            [date, '23:05'],
            [date],
        ];

        expect(posts).toHaveLength(6);
        posts.forEach(post => post.props().clickHandler());

        expect(cardClickHandler.mock.calls.length).toBe(6);

        expectedParams.forEach((ep, i) =>
            expect(cardClickHandler.mock.calls[i]).toEqual(ep)
        );
    });
});
