import React from 'react';

import classes from './Backdrop.module.scss';

const backdrop = props => (
    <div className={classes.Backdrop} onClick={props.clickHandler} />
);

export default backdrop;
