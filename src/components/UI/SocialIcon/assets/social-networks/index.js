export { default as facebookBw } from './facebook-bw.svg';
export { default as facebookColor } from './facebook-color.svg';

export { default as gplusBw } from './gplus-bw.svg';
export { default as gplusColor } from './gplus-color.svg';

export { default as instagramBw } from './instagram-bw.svg';
export { default as instagramColor } from './instagram-color.svg';

export { default as twitterBw } from './twitter-bw.svg';
export { default as twitterColor } from './twitter-color.svg';
