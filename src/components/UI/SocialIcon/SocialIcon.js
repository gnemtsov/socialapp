import React from 'react';
import PropTypes from 'prop-types';

import * as socialIcons from './assets/social-networks';
import classes from './SocialIcon.module.scss';

/*TODO
The following code should be removed (and ./assets/accounts folder too) when the component will be integrated into the app. Instead of using accountsIcons map we should create links to avatars just like this:
http://our-cdn/${props.account.toLowerCase()}-color.svg
*/
//---
import * as accountsIcons from './assets/accounts';
//---

const socialIcon = props => {
    let iconClasses = [classes.Icon];

    if (props.class !== undefined) {
        iconClasses.push(props.class);
    }

    let tabIndex;

    let accountIcon =
        'url(' + accountsIcons[`${props.account.toLowerCase()}Color`] + ')'; //TODO replace with CDN link
    let miniatureIcon =
        'url(' + socialIcons[`${props.network.toLowerCase()}Color`] + ')';
    let curtainIcon;

    let clickHandler;
    let mouseEnterHandler;
    let mouseLeaveHandler;
    let keyDownHandler;

    if (props.type === 'dynamic') {
        iconClasses.push(classes.Dynamic);
        tabIndex = props.tabIndex;

        if (props.selected) {
            iconClasses.push(classes.Selected);
            curtainIcon = 'none';
        } else {
            accountIcon =
                'url(' +
                accountsIcons[`${props.account.toLowerCase()}Bw`] +
                ')'; //TODO replace with CDN link
            curtainIcon =
                'linear-gradient(rgba(255, 255, 255, 0.7), rgba(255, 255, 255, 0.7))';
        }

        clickHandler = () => {
            props.clickHandler(props.account, props.network);
        };

        mouseEnterHandler = event => {
            event.currentTarget.focus();
        };

        mouseLeaveHandler = event => {
            event.currentTarget.blur();
        };

        keyDownHandler = event => {
            if ([13, 32].includes(event.keyCode)) {
                //trigger click for enter or space
                event.preventDefault();
                event.target.click();
            }
        };
    } else {
        curtainIcon =
            'url(' + socialIcons[`${props.network.toLowerCase()}Bw`] + ')';
        iconClasses.push(classes.Static);
    }

    return (
        <div
            data-role="social-icon"
            tabIndex={tabIndex}
            className={iconClasses.join(' ')}
            style={{ backgroundImage: accountIcon }}
            title={props.account[0].toUpperCase() + props.account.slice(1)}
            onMouseEnter={mouseEnterHandler}
            onMouseLeave={mouseLeaveHandler}
            onKeyDown={keyDownHandler}
            onClick={clickHandler}
        >
            <div
                key="social-icon"
                data-role="social-miniature"
                className={classes.Miniature}
                style={{ backgroundImage: miniatureIcon }}
            />
            <div
                key="social-curtain"
                data-role="social-curtain"
                className={classes.Curtain}
                style={{ backgroundImage: curtainIcon }}
            />
        </div>
    );
};

socialIcon.propTypes = {
    type: PropTypes.oneOf(['dynamic', 'static']),
    account: PropTypes.string.isRequired,
    network: PropTypes.string.isRequired,
    selected: PropTypes.bool,
    clickHandler: PropTypes.func,
};

export default socialIcon;
