import React, { Component } from 'react';

import Popup from '../Popup/Popup';

import classes from './ErrorBoundary.module.scss';

class ErrorBoundary extends Component {
    static isActive = false;

    constructor(props) {
        super(props);
        this.state = {
            errorSource: null,
            error: null,
            info: null,
        };
    }

    reportError = () => {
        //const { errorSource, error, state } = this.state;
        if (process.env.NODE_ENV === 'production') {
            //TODO report errors somewhere
            return;
        }
    };

    componentDidCatch(error, info) {
        this.setState(
            {
                errorSource: 'client',
                error,
                info,
            },
            this.reportError
        );
    }

    errorConfirmedHandler = () => {
        ErrorBoundary.isActive = false;
        this.setState({ errorSource: null, error: null, info: null });
    };

    render() {
        let popup = null;
        let message = '';
        if (this.state.errorSource !== null && !ErrorBoundary.isActive) {
            ErrorBoundary.isActive = true;
            message =
                process.env.NODE_ENV === 'development'
                    ? `Oops, we've got a ${
                          this.state.errorSource
                      }-side error(s)!\n See developer console for details...`
                    : `Something went wrong!`;
            popup = (
                <Popup title="Error" closeHandler={this.errorConfirmedHandler}>
                    <div className="classes.ErrorContainer">
                        <span className="classes.ErrorMessage">{message}</span>
                    </div>
                </Popup>
            );
        }

        let children = this.props.children;
        if (this.state.error !== null) {
            children = <div className={classes.BrokenComponent}>Error :(</div>;
        }

        return (
            <React.Fragment>
                {popup}
                {children}
            </React.Fragment>
        );
    }
}

export default ErrorBoundary;
