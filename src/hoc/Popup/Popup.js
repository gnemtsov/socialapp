import React from 'react';
import PropTypes from 'prop-types';

import Backdrop from '../../components/UI/Backdrop/Backdrop';

import classes from './Popup.module.scss';

const popup = props => {
    return (
        <React.Fragment>
            <div
                className={classes.Popup}
                role="dialog"
                aria-modal="true"
                onKeyDown={e => e.keyCode === 27 && props.closeHandler(e)}
            >
                <header className={classes.PopupHeader}>
                    <div className={classes.PopupTitle}>{props.title}</div>
                    <button
                        type="button"
                        className={classes.Close}
                        onClick={props.closeHandler}
                    />
                </header>
                <div className={classes.PopupBody}>{props.children}</div>
            </div>
            <Backdrop show clickHandler={props.closeHandler} />
        </React.Fragment>
    );
};

popup.defaultProps = {
    title: 'Popup',
};

popup.propTypes = {
    title: PropTypes.string,
    closeHandler: PropTypes.func.isRequired,
};

export default popup;
