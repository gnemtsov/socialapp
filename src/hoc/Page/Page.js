import React, { Component } from 'react';

import classes from './Page.module.scss';
import Menu from '../../components/Menu/Menu';
import SideDrawer from '../../components/Menu/SideDrawer/SideDrawer';

class Page extends Component {
    state = {
        showSideDrawer: false,
    };

    sideDrawerClosedHandler = () => {
        this.setState({ showSideDrawer: false });
    };

    sideDrawerToggleHandler = () => {
        this.setState(state => {
            return { showSideDrawer: !state.showSideDrawer };
        });
    };

    render() {
        return (
            <React.Fragment>
                <Menu drawerToggleClicked={this.sideDrawerToggleHandler} />
                <SideDrawer
                    open={this.state.showSideDrawer}
                    closed={this.sideDrawerClosedHandler}
                />
                <main className={classes.Main}>{this.props.children}</main>
            </React.Fragment>
        );
    }
}

export default Page;
