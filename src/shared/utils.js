export let getCopy = input => {
    let copy;

    if (Array.isArray(input)) {
        copy = [];
        for (const prop of input) {
            copy[prop] = getCopy(input[prop]);
        }
    } else if (typeof input === 'object' && input !== null) {
        copy = {};
        for (const prop in input) {
            if (input.hasOwnProperty(prop)) {
                copy[prop] = getCopy(input[prop]);
            }
        }
    } else {
        copy = input;
    }

    return copy;
};

export let compareArrays = (ar1, ar2) =>
    ar1.length === ar2.length &&
    ar1.sort().every((v, i) => v === ar2.sort()[i]);
