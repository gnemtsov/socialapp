//Settings
//For days settings a more specific setting wins
const settings = {
    accountsNetworks: {
        tesla: 'facebook,gplus,instagram,twitter',
    },
    dateSelectFormat: 'MMMM, D, ddd',
    dateTitleDisplayFormat: 'MMMM, D',
    days: {
        '2018-12-25': {
            retweets: '160',
            likes: '300',
            replies: '200',
            slots: {
                '14:00': {
                    content: `First slot`,
                },
            },
        },
        Friday: {
            likes: '500',
            slots: {
                '11:00': {
                    content: `First slot`,
                },
                '16:00': {
                    content: `First slot`,
                },
            },
        },
        default: {
            retweets: '60',
            likes: '200',
            replies: '100',
            slots: {
                '10:00': {
                    content: `First slot`,
                },
                '14:00': {
                    content: `Second slot`,
                },
                '23:05': {
                    content: `Third slot`,
                },
            },
        },
    },
};

export default settings;
